
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">EasyAdmin v1.0.0</h1>
<h4 align="center">中小企业快速开发框架</h4>


## 平台简介

EasyAdmin是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用，一键生成前后台代码和菜单权限，
中小企业快速开发脚手架。

* 前端采用Vue、Element UI、LayUI。
* 后端采用Spring Boot、Mysql、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。

## 本地部署
* 本地导出doc文件夹下面easy-admin.sql脚本并修改application-dev.yml数据配置文件信息
* 修改application-dev.yml的redis的账号和密码
* 运行EasyAdminApplication启动类即可
* 本地访问 http://localhost:8901/login

## 账号密码
* 管理员:admin 密码:123456
## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql） 。
14. 快速建表：前端直接动态建表操作，无需mysql基础也可以操作 。
15. 一键部署：零基础小白直接一键部署代码，无需任何操作，方便快捷 。
16. 消息推送：websocket后台管理员可以推送消息给所有用户。
15. 系统接口：根据业务代码自动生成相关的api接口文档。
16. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。

## 演示图

<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207132527.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113500.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113510.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113516.png"/>

<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113520.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113529.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113533.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113537.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113545.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113552.png"/>

<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113556.png"/>
<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113615.png"/>

<img src="https://mars-factory.oss-cn-beijing.aliyuncs.com/image1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231207113618.png"/>



