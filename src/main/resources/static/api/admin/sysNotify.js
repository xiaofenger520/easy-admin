/**
 * 分页查询通知公告列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysNotify/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询通知公告详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysNotify/query/' + id,
        method: 'get'
    })
}

/**
 * 新增通知公告
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysNotify/add',
        method: 'post',
        data: data
    })
}

/**
 * 推送通知
 *
 * @param data
 * @returns {*}
 */
function pushNotify(data) {
    return requests({
        url: '/admin/sysNotify/push',
        method: 'post',
        data: data
    })
}

/**
 * 修改通知公告
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysNotify/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除通知公告
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysNotify/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出通知公告
 *
 * @param query
 * @returns {*}
 */
function exportNotify(query) {
    return requests({
        url: '/admin/sysNotify/export',
        method: 'get',
        params: query
    })
}
