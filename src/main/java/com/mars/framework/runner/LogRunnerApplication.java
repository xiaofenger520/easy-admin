package com.mars.framework.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetAddress;

/**
 * @author wq
 * @description: 启动日志
 * @date 2023/06/21
 * @version: 1.0
 */
@Slf4j
@Component
public class LogRunnerApplication implements ApplicationRunner{

    @Resource
    private Environment env;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String applicationName = env.getProperty("spring.application.name");
        String path = env.getProperty("server.servlet.context-path") != null ? env.getProperty("server.servlet.context-path") : "";
        log.info("\n----------------------------------------------------------\n\t" +
                "Application " + applicationName + " is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/login\n\t" +
                "External: \thttp://" + ip + ":" + port + path + "/login\n\t" +
                "Api Doc: \thttp://" + ip + ":" + port + path + "/doc.html\n\t" +
                "----------------------------------------------------------");
    }



}
