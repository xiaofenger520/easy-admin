package com.mars.common.request.sys;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 角色DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRoleRequest {

    private Long id;

    private String roleName;

    private String remark;

    private LocalDateTime createTime;


}
