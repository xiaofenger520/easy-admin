package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApTest;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 测试Mapper接口
 *
 * @author mars
 * @date 2023-12-05
 */
public interface ApTestMapper extends BasePlusMapper<ApTest> {

}
