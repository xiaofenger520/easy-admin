package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.SysOssRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.SysOssMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.SysOss;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.ISysOssService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 文件存储业务层处理
 *
 * @author mars
 * @date 2023-11-20
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysOssServiceImpl implements ISysOssService {

    private final SysOssMapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysOss add(SysOssRequest request) {
        SysOss entity = new SysOss();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(SysOssRequest request) {
        SysOss entity = new SysOss();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public SysOss getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<SysOss> pageList(SysOssRequest request) {
        Page<SysOss> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysOss> query = this.buildWrapper(request);
        IPage<SysOss> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<SysOss> buildWrapper(SysOssRequest param) {
        LambdaQueryWrapper<SysOss> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(SysOss::getName ,param.getName());
        }
         if (StringUtils.isNotBlank(param.getEndpoint())){
               query.like(SysOss::getEndpoint ,param.getEndpoint());
        }
         if (StringUtils.isNotBlank(param.getAccessKey())){
               query.like(SysOss::getAccessKey ,param.getAccessKey());
        }
         if (StringUtils.isNotBlank(param.getSecretKey())){
               query.like(SysOss::getSecretKey ,param.getSecretKey());
        }
         if (StringUtils.isNotBlank(param.getBucketName())){
               query.like(SysOss::getBucketName ,param.getBucketName());
        }
        return query;
    }

}
