package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApTestRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApTestMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApTest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApTestService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 测试业务层处理
 *
 * @author mars
 * @date 2023-12-05
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApTestServiceImpl implements IApTestService {

    private final ApTestMapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApTest add(ApTestRequest request) {
        ApTest entity = new ApTest();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApTestRequest request) {
        ApTest entity = new ApTest();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApTest getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApTest> pageList(ApTestRequest request) {
        Page<ApTest> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApTest> query = this.buildWrapper(request);
        IPage<ApTest> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<ApTest> buildWrapper(ApTestRequest param) {
        LambdaQueryWrapper<ApTest> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApTest::getName ,param.getName());
        }
         if (StringUtils.isNotBlank(param.getPicture())){
               query.like(ApTest::getPicture ,param.getPicture());
        }
        return query;
    }

}
