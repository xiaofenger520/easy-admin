package com.mars.module.system.controller;

import com.mars.common.response.sys.CaptchaResponse;
import com.mars.common.result.R;
import com.mars.module.system.service.ICaptchaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wangqiang
 * @version 1.0
 * @date 2021/3/6 22:15
 */
@RestController
@RequestMapping("/captcha")
@Api(tags = "验证码接口")
@AllArgsConstructor
public class CaptchaController {

    private final ICaptchaService captchaService;


    /**
     * 生成验证码
     */
    @ApiOperation(value = "获取验证码")
    @GetMapping("/acquire")
    public R<CaptchaResponse> acquire() {
        return R.success(captchaService.acquire());
    }
}
